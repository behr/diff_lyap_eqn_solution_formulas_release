[CMESS]: https://gitlab.mpi-magdeburg.mpg.de/mess/cmess
[MATLAB]: https://de.mathworks.com/
[CSCBIB]: https://gitlab.mpi-magdeburg.mpg.de/csc-administration/csc-bibfiles
[RELEASE]: https://gitlab.mpi-magdeburg.mpg.de/behr/diff_lyap_eqn_solution_formulas_release

[GPLV2]: https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
[BGPLV2]: https://img.shields.io/badge/License-GPL%20v2-blue.svg

[GPLV3]: https://www.gnu.org/licenses/gpl-3.0
[BGPLV3]: https://img.shields.io/badge/License-GPL%20v3-blue.svg

[ZENODO]: https://zenodo.org/badge/doi/10.5281/zenodo.1484327.svg

[LTVBT]: https://doi.org/10.5281/zenodo.834953




# Solution Formulas for Differential Sylvester and Lyapunov Equations
Latex Files and `Matlab` Code for the Preprint.

**Version: 1.1**

**License:**

[![License: GPL v2][BGPLV2]][GPLV2]     [![License: GPL v3][BGPLV3]][GPLV3]

[![Gitter chat](https://badges.gitter.im/gitterHQ/gitter.png)](https://gitter.im/yoda90/Lobby#) **Copyright 2016-2018 by Maximilian Behr (MPI Magdeburg)**


[![pipeline status](https://gitlab.mpi-magdeburg.mpg.de/paper-collection/2019/diff_lyap_eqn_solution_formulas/badges/master/pipeline.svg)](https://gitlab.mpi-magdeburg.mpg.de/paper-collection/2019/diff_lyap_eqn_solution_formulas/commits/master)

[![ZENODO]](https://zenodo.org/record/1484327)


The [MATLAB][MATLAB] Code is licensed under `GPL v2` or later.
See [LICENSE](LICENSE) for details.

## Requirements
This Project depends on several other `git` repositories:

* [C-M.E.S.S][CMESS] (`GPL v2` or later, Copyright 2009 - 2018 by Peter Benner, Martin Koehler, Jens Saak (MPI Magdeburg)) and it`s submodules
* [csc-bibfiles][CSCBIB]
* [diff_lyap_eqn_solution_formulas_release][RELEASE]


These repositories are added as submodules to this project.
Therefore you have to clone this project using:

```
git clone --recursive git@gitlab.mpi-magdeburg.mpg.de:behr/diff_lyap_eqn_solution_formulas.git
```

The project needs the [MATLAB][MATLAB] interface of [C-M.E.S.S][CMESS]. You have to build it before using the codes.

For the $`LDL^T`$ numerical experiments you need the [LTV-BT for MATLAB][LTVBT] Differential Lyapunov
solvers [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.834953.svg)](https://doi.org/10.5281/zenodo.834953).
When you execute the `add_to_path.m` script the Code is downloaded automatically.




## Describtion of the Directories and Files

|   Directory / File                                        |   Describtion                                                                                                                                             |
|:---------------------------------------------------------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------:|
|   /release                                                |   Code for Preprint                                                                                                                                       |
|   /release/cmess-release                                  |   [C-M.E.S.S][CMESS] subproject                                                                                                                           |
|   /release/build_cmess.sh                                 |   Bash script, to compile [C-M.E.S.S][CMESS]                                                                                                              |
|   /release/add_to_path.m                                  |   [MATLAB][MATLAB] script to adapth the path                                                                                                              |
|   /release/data                                           |   Test Matrices not included                                                                                                                              |
|   /release/helpers                                        |   functions for miscallenous tasks                                                                                                                        |
|   /release/mmread                                         |   `mtx` read and write functions                                                                                                                          |
|   /release/lyap_spectral_solver                           |   Algebraic / Differential Lyapunov Solver based on the spectral decomposition                                                                            |
|   /release/z_projection                                   |   Projection Based approach for differential Lyapunov equation                                                                                            |
|   /release/scenario/generate_z_proj_rail_error.sh         |   Bash script to generate [MATLAB][MATLAB] scripts, job files and a bunch of shell scripts, to run the codes with different solvers ect.                  |
|   /release/scenario/clean_generated.sh                    |   Bash script to delete all generated files from generate_z_proj_rail_error.sh                                                                            |
|   /release/ldlt/ldlt_bench.m                              |   [MATLAB][MATLAB] script to generate the numerical results for comparison with $`LDL^T'$ solver                                                          |

## Reproduce Results

### Get files for `data` directory.

The `data` directory contains the rail matrices saved as `mtx` and `mat` files.

The `data` directory contains also the  (generalized) spectral decomposition of the rail matrices.

You can download the files here:
* https://zenodo.org/record/1165443#.WoRLqeYxlUc

Place the matrices directly in the `data` directory of the cloned git repository.

You can also compute the spectral decompositions on your own by calling:

```
    rail2mat
    run_rail_eigensystem
```


### Compile [MATLAB][MATLAB] interface to [C-M.E.S.S][CMESS]

* change to `CODE/release` directory
* exectue the script `build_cmess.sh` or compile [C-M.E.S.S][CMESS] and the corresponding [MATLAB][MATLAB] interface on your own

### Run [MATLAB][MATLAB] tests

* open [MATLAB][MATLAB]
* change to `release` directory
* execute `add_to_path.m` script this add the necessary directories to the [MATLAB][MATLAB] path
* call `run_diff_lyap_tests` this a test for the solver in `lyap_spectral_solver`


### Projection Based Approach

All codes for the projection based approached are in `release/z_projection/`.

Change to the subdirectory `scenario` and exectue the Bash script `generate_z_proj_rail_error.sh`.
It will generate a bunch of [MATLAB][MATLAB] scripts, Job Files and Bash Scripts.

Each script uses the projection based approach to solve the Differential Lyapunov equation and the solution from the spectral decomposition is used as a reference solution for error computation.

After a succesfull run results have been written to a new folder results.

### $`LDL^T`$ Based Solver

All codes for the $`LDL^T`$ solver are in `release/ldlt/`.

Execute the script `ldlt_bench.m` and the results are written to a new directory called `results`.


# Testing

The Code was tested with:

* **MATLAB 2013b**
* **MATLAB 2014b**
* **MATLAB 2015b**
* **MATLAB 2016b**
* **MATLAB 2017a**

# Numerical Results

The results for the paper were produced on CSC Computer Servers:

* [**Bruno, 24.01.2018**](https://gitlab.mpi-magdeburg.mpg.de/behr/diff_lyap_eqn_solution_formulas/uploads/5e294c88d7d24fcc36bfa7d0d0e841ff/z_projection_results.tar.gz)
    * unpack the tar file
    * it contains several folders: `rail[SIZE]_[ODESOLVERS]_op_[NONE/TRANS]_[TRUE/FALSE]_local_parallel_FOURIER_[TRUE/FALSE]_SPECTRALDECOMP_[TRUE/FALSE]`
    * `[SIZE]` indicates the size of the rail instance
    * `[ODESOLVER]` indicates the ode solver which was used
    * `op_[NONE/TRANS]` indicates the transposed or non transposed equation
    * `[TRUE/FALSE]_local_parallel` indicates whether the ODEs are solved in parallel columnwise or not
    * `FOURIER_[TRUE/FALSE]` indicates if a generalized projected ODEs system or a standard projected ODE system is solved
    * `SPECTRALDECOMP_[TRUE/FALSE]`  indicates if the precompted spectral decomposition is loaded from `*.mat` file in data directory or computed during the call

