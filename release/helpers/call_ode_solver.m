% Wrapper around ode solver call
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20017-2018
%

function varargout = call_ode_solver(solver,varargin)

switch solver
    case 'ode45'
        [varargout{1:nargout}] = ode45(varargin{:});
    case 'ode23'
        [varargout{1:nargout}] = ode23(varargin{:});
    case 'ode113'
        [varargout{1:nargout}] = ode113(varargin{:});
    case 'ode15s'
        [varargout{1:nargout}] = ode15s(varargin{:});
    case 'ode23s'
        [varargout{1:nargout}] = ode23s(varargin{:});
    case 'ode23t'
        [varargout{1:nargout}] = ode23t(varargin{:});
    case 'ode23tb'
        [varargout{1:nargout}] = ode23tb(varargin{:});
    case 'ode15i'
        [varargout{1:nargout}] = ode15i(varargin{:});
    otherwise
        error('%s solver not available.',solver);

end

