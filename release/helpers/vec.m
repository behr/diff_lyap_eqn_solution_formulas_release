function v = vec(X)
% vec operator unrolls  n-x-m matrix to n*m vector by stacking columns
%
% Calling Sequences:
%   v = vec(X):
%
% Input:
%   X     - matrix of size n-x-m
%
% Output:
%   v     - vector of size n*m (vectorized matrix)
%
%

%n = numel(X);
%assert(n>0,strcat(inputname(1),' contains 0 elements'));
%v = reshape(X,numel(X),1);
v = X(:);
end
