function X = invvec(v,n,m)
% invvec shrinks  n*m vector to n-x-m matrix.
%
% Calling Sequences:
%   X = invvec(v):
%   It is assumed that the number of entries of v is a square number n^2.
%   The resulting matrix is of size n-x-m.
%
%   X = invvec(v,m,n)
%
%
% Input:
%   v     - vector of size n*m
%   n     - (optional) number of rows of matrix
%   m     - (optional) number of cols of matrix
%
% Output:
%   X     - reshaped vector of matrix X
%
%

%% check input
assert(nargin==1 || nargin == 3, 'invvec:wrong calling sequence.');
if nargin >= 3
    assert(n>0, sprintf('%s is not positive.', inputname(2)));
    assert(m>0, sprintf('%s is not positive.', inputname(3)));
    assert(numel(v)==n*m, ...
        sprintf('numel(%s) doest not fit to %s*%s', ...
        inputname(1), inputname(2), inputname(3) ...
        )   ...
        );
end


%% convert vector to matrix
if nargin == 1
    n = round(sqrt(length(v)));
    X = reshape(v,n,n);
else
    X = reshape(v,n,m);
end
