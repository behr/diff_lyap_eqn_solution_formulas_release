function [ret,num,unit] = ByteSize(in, fid)
% BYTESIZE writes the memory usage of the provide variable to the given file
% identifier. Output is written to screen if fid is 1, empty or not provided.

if nargin == 1 || isempty(fid)
    fid = 1;
end

s = whos('in');
%fprintf(fid,[Bytes2str(s.bytes) '\n']);
[num,unit] = Bytes2str(s.bytes);
ret = sprintf('%.4f %s\n',num,unit);

end

function [num,unit] = Bytes2str(NumBytes)
% BYTES2STR Private function to take integer bytes and convert it to
% scale-appropriate size.

scale = floor(log(NumBytes)/log(1024));
switch scale
    case 0
        num     = NumBytes;
        unit    = 'b';
    case 1
        num = NumBytes/(1024);
        unit    = 'kb';
    case 2
        num = NumBytes/(1024)^2;
        unit    = 'Mb';
    case 3
        num = NumBytes/(1024)^3;
        unit    = 'Gb';
    case 4
        num = NumBytes/(1024)^4;
        unit    = 'Tb';
    case -inf
        % Size occasionally returned as zero (eg some Java objects).
        num = 0;
        unit = 'N/A';
    otherwise
        num = 0;
        unit = 'N/A';
        warning(Byte2str,'Over a petabyte!!!');
end
end
