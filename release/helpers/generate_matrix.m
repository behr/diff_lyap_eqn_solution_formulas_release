% GENERATE_MATRIX, generates a random matrix of
% given data type
%
% Input:
% n     - Number of Rows,       n >= 1
% m     - Number of Columns,    m >= 0
% dtype - Datatype of Matrix,
%         0 - double matrix
%         1 - complex matrix
%
% Output:
% M     - Matrix of datatype dtype with n Rows and m Cols.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20017-2018
%

function M = generate_matrix(n,m,dtype)

% check input parameters
assert(n>=0, 'n is negative');
assert(m>=0, 'm is negative');
assert(dtype==0 || dtype==1, 'dtype is not 0 or 1');

% compute random matrix
if dtype == 0
    M = rand(n,m);
else
    M = .5*(rand(n,m) + 1i*rand(n,m));
end
end
