% Script for comparison with ldlt on shorter time horizon
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20016-2018
%



%% load and setup data
railname = 'rail1357';
rail = load(railname);
E       = rail.E;
A       = rail.A;
B       = rail.B;
C       = rail.C;


%% load spectral decomposition of rail matrices
if true
    rail_spectral_decomp = load('rail1357_spectral_decomp');
end
% please note that the rail matrix example is symmetric therefore
% the same spectral decomposition can be used for both cases



%% E \dot{X} E' = A X E' + E' X A + BB' , X(0)=0

%create configuration and parameters, and spectral decompositition
z_projection_conf_n                         = z_projection_conf();
z_projection_conf_n.name                    = 'rail1357_ode15s_op_none_ldlt_comp';
z_projection_conf_n.ode_solver              = 'ode15s';
z_projection_conf_n.mess_opt.type           = mess_operation_t.MESS_OP_NONE;
if false
    z_projection_conf_n.cluster             = parcluster('%CLUSTER%');
else
    z_projection_conf_n.cluster             = [];
end
z_projection_conf_n.generalized_fourier     = false;
z_projection_conf_n.tspan                   = 0:1/2:100;

% create solver via spectral decomposition
if true
    % use precomputed spectral decomposition
    lyap_spectral_n = lyap_spectral_solver(E,A,B,true,rail_spectral_decomp.VAE, rail_spectral_decomp.DAE);
else
    % spectral decomposition is computed during lyap_spectral_solver
    lyap_spectral_n  = lyap_spectral_solver(E,A,B,true);
end


%call z projection
z_projection_error(E,A,B,z_projection_conf_n,lyap_spectral_n);


%% E' \dot{X} E = A' X E + E X A' + C'C , X(0)=0

% create configuration and parameters, and spectral decompositition
z_projection_conf_t                         = z_projection_conf();
z_projection_conf_t.name                    = 'rail1357_ode15s_op_trans_ldlt_comp';
z_projection_conf_t.ode_solver              = 'ode15s';
z_projection_conf_t.mess_opt.type           = mess_operation_t.MESS_OP_TRANSPOSE;
if false
    z_projection_conf_t.cluster             = parcluster('%CLUSTER%');
else
    z_projection_conf_t.cluster             = [];
end
z_projection_conf_t.generalized_fourier     = false;
z_projection_conf_t.tspan                   = 0:1/2:100;


% create solver via spectral decomposition
if true
    % use precomputed spectral decomposition
    lyap_spectral_t = lyap_spectral_solver(E',A',C',true,rail_spectral_decomp.VAE, rail_spectral_decomp.DAE);
else
    % spectral decomposition is computed during lyap_spectral_solver
    lyap_spectral_t  = lyap_spectral_solver(E',A',C',true);
end


% call z projection
z_projection_error(E,A,C,z_projection_conf_t,lyap_spectral_t);



