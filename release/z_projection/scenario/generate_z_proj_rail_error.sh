#!/bin/bash

#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               20016-2018
#


# this script generates m files for testing
RAILNAMES=("rail371" "rail1357" "rail5177" "rail20209" "rail79841" )

# spectral decomposition loaded or not
FROMSPECTRALDECOMPOSITIONS=("true" "false")

# ode solvers
ODESOLVERS=("ode45" "ode23" "ode113" "ode15s" "ode23s" "ode23t" "ode23tb")

# MATLAB version
MATLAB=matlab2015b

# parallel settings
CLUSTER="local"
CLUSTERNAME=local_parallel
PARALLELS=("true" "false")

# generalized fourier
GENFOURIER=false

# time settings
TSPAN="unique([linspace(0,10,100),linspace(10,4500,150)])"

QUEUE=long

for PARALLEL in "${PARALLELS[@]}"; do
    for FROMSPECTRALDECOMPOSITION in "${FROMSPECTRALDECOMPOSITIONS[@]}"; do
        for RAILNAME in "${RAILNAMES[@]}"; do

            # copy file and do replacements for job file
            JNAME=z_proj_${RAILNAME}_${PARALLEL}_${CLUSTERNAME}_decomp_${FROMSPECTRALDECOMPOSITION}.jobfile
            cp z_proj_rail_error.template_jobfile ${JNAME} &
            wait

            # do replacements in jobfile
            sed -i "s/%QUEUE%/${QUEUE}/g"                   ${JNAME}
            sed -i "s/%JNAME%/${JNAME}/g"                   ${JNAME}

            # copy file and do replacements for bash script file
            SCRIPTNAME=z_proj_${RAILNAME}_${PARALLEL}_${CLUSTERNAME}_decomp_${FROMSPECTRALDECOMPOSITION}.sh
            cp z_proj_rail_error.template_bash ${SCRIPTNAME} &
            wait


            for ODESOLVER in "${ODESOLVERS[@]}"; do

                # copy file and do replacements for m file
                MNAME=z_proj_${RAILNAME}_${ODESOLVER}_${PARALLEL}_${CLUSTERNAME}_decomp_${FROMSPECTRALDECOMPOSITION}.m
                cp z_proj_rail_error.template ${MNAME} &
                wait

                # do replacements
                sed -i "s/%RAILNAME%/${RAILNAME}/g"                                     ${MNAME}
                sed -i "s/%ODESOLVER%/${ODESOLVER}/g"                                   ${MNAME}
                sed -i "s/%CLUSTERNAME%/${CLUSTERNAME}/g"                               ${MNAME}
                sed -i "s/%CLUSTER%/${CLUSTER}/g"                                       ${MNAME}
                sed -i "s/%GENFOURIER%/${GENFOURIER}/g"                                 ${MNAME}
                sed -i "s/%TSPAN%/${TSPAN}/g"                                           ${MNAME}
                sed -i "s/%FROMSPECTRALDECOMPOSITION%/${FROMSPECTRALDECOMPOSITION}/g"   ${MNAME}
                sed -i "s/%PARALLEL%/${PARALLEL}/g"                                     ${MNAME}

                # append my jobs
                echo "matlab -nodesktop -nosplash -r \"run ../../add_to_path; run ${MNAME};exit(0)\";" >> ${JNAME}

                # append to bash script
                echo "${MATLAB} -nodesktop -nosplash -r \"run ../../add_to_path; run ${MNAME};exit(0)\";" >> ${SCRIPTNAME}

            done
        done
    done
done

chmod aug+x *.sh
