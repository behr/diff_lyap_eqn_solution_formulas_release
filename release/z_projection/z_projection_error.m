function results = z_projection_error(E,A,B,z_projection_conf, true_solver)
%% This function computes the solution of the Differential Lyapunov
%  equation with the ansatz z projection.
%
%  The DLE
%
%  (MESS_OP_NONE)       Edot{X}E' = AXE' +EXA' + BB', X(0)=0
%
%  or
%
%  (MESS_OP_TRANSPOSE)  E'dot{X}E = A'XE +E'XA + B'B, X(0)=0
%
%  This class uses mex-mess to solve the algebraic Lyapunov Equation (ALE)
%
%  (MESS_OP_NONE)       0 = AZ_{\infty}Z_{\infy}'E' + EZ_{\infty}Z_{\infty}'A' + BB'
%
%  or
%
%  (MESS_OP_TRANSPOSE)  0 = A'Z_{\infty}Z_{\infty}'E + E'Z_{\infty}Z_{\infty}'A + B'B
%
%  The Solution is given by X(t) = Z_{\infty}*Z_{\infty}' - Z(t)*Zt'.
%  and Zt solves
%
%  (MESS_OP_NONE)       E \dot{Z(t)} = A Z(t), Z(0)=Z_{\infty}
%
%  or
%
%  (MESS_OP_TRANSPOSE)  E'\dot{Z(t)} = A' Z(t), Z(0)=Z_{\infty}
%
%  Since
%
%  (MESS_OP_NONE)           Z(t) = e^(tE^(-1)A)Z_{\infty}
%
%  or
%
%  (MESS_OP_TRANSPOSE)      Z(t) = e^(tE^(-T)A^T)Z_{\infty}
%
%  and the columns  of Z_{\infty} contain a invariant subspace for
%
%  (MESS_OP_NONE)           E^(-1)A
%
%  or
%
%  (MESS_OP_TRANSPOSE)      E^(-T)A^T
%
%  we can write Z(t) = Q_{\infty} z(t) with Z_{\infty} = Q_{\infty} R_{\infty}.
%  This leads to the ode for z(t)
%
%  (MESS_OP_NONE)           E Q_{\infty}\dot{z}(t) = A Q_{\infty}
%
%                               z(0) = Q_{\infty}'Z_{\infty} = R_{\infty}
%   or
%
%  (MESS_OP_TRANSPOSE)      E'Q_{\infty} \dot{z}(t) = A' Q_{\infty} z(t)
%
%                               z(0) = Q_{\infty}'Z_{\infty} = R_{\infty}
%
%  This leads to the ode for z(t)
%
%  (MESS_OP_NONE)          dot{z}(t) = Q_{\infty}^(T)E^(-1)A Q_{\infty}
%
%                               z(0) = Q_{\infty}'Z_{\infty} = R_{\infty}
%   or
%
%  (MESS_OP_TRANSPOSE)    \dot{z}(t) = Q_{\infty}^(T) E^(-T)A' Q_{\infty} z(t)
%
%                               z(0) = Q_{\infty}'Z_{\infty} = R_{\infty}
%
%
%
%  For comparision we need a function or object true_solver
%  which can return the true solution for given time t.
%  We integrate the ODE above in parallel and divide the number of columns
%  to the workers.
%  Afterwards the absolute and relative error in the 2-Norm is computed
%  and the error and log files are written to disk.
%
%  All options can be configured using z_projection_conf instance.
%
%  Inputs:
%
%   E                   - (Input) System Matrix [] if E=I
%   A                   - (Input) System Matrix
%   B                   - (Input) System Matrix
%   z_projection_conf   - (Input) Configuration for all options
%   true_solver         - (Input) an instance which has the field or function
%                         solve_dlyap(t) which returns the true (precomputed) solution
%
% Maximilian Behr, MPI-Magdeburg

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20016-2018
%

%% check input
assert(isa(z_projection_conf,'z_projection_conf'),'no z_projection_config object')

%% create necessary filenames and create directory for results
%mydir   = sprintf('results/%s/%s/',mfilename,z_projection_conf.name);
[mydir,~,~] = fileparts(mfilename('fullpath'));
mydir       = fullfile(mydir,'results',mfilename,z_projection_conf.name);
mydiary     = fullfile(mydir,sprintf('%s.log',z_projection_conf.name));
mymat       = fullfile(mydir,sprintf('%s.mat',z_projection_conf.name));
mydat       = fullfile(mydir,sprintf('%s.dat',z_projection_conf.name));
mkdir(mydir);

%% delete old diary and create new one
delete(mydiary);
diary(mydiary);

%% call mess_version to get its output logged by diary
mess_version();

%% create results structure
results     = struct();

%% options for ODE Solver
tspan       = z_projection_conf.tspan;
ode_opt     = z_projection_conf.ode_opt;
ode_solver  = z_projection_conf.ode_solver;

%% options for mex-mess
mess_opt    = z_projection_conf.mess_opt;

%% parallel environemnt
cluster     = z_projection_conf.cluster;

%% generalized fourier
generalized_fourier = z_projection_conf.generalized_fourier;

%% create z_projection object
z_proj   = z_projection(E,A,B,mess_opt,ode_solver,ode_opt,tspan,cluster,generalized_fourier);

%% solve lyapunov equation
z_proj.solve_lyap();

%% perform svd
z_proj.svd_Zinf();


%% compute fourier system
z_proj.compute_fourier_system();

%% solve fourier system / solve for differential part
tic;
z_proj.solve_fourier_system();
fprintf('%s Start Integration\n',datestr(now));
time_integration=toc;

%% compute residual from lyapunov equation
[Zabs2res, Zrel2res,ZnrmRHS] = z_proj.res2_lyap();

%% compute error
if ~isempty(true_solver)

    %% create handle which can compute the true error
    get_true_solution = @(t) true_solver.solve_dlyap(t);

    %% compute 2-norm error
    fprintf('%s Compute Error\n',datestr(now));
    [nrm2err, nrm2Xtrue, nrm2ZmZtime, nrm2Ztime] = z_proj.compute_error(get_true_solution);

else
    warning('Cannot Compute Error no true solver given.');
end


%% results for integration
results.time_integration    = time_integration;
results.ode_solver          = ode_solver;
results.tspan               = tspan;
results.absnrm2err          = nrm2err;
results.nrm2Xtrue           = nrm2Xtrue;

temp                        = nrm2err./nrm2Xtrue; temp(isinf(temp))=0; temp(isnan(temp))=0;
results.relnrm2err          = temp;
results.maxabsnrm2err       = max(results.absnrm2err);
results.meanabsnrm2err      = mean(results.absnrm2err);

results.maxrelnrm2err       = max(results.relnrm2err);
results.meanrelnrm2err      = mean(results.relnrm2err);

results.nrm2ZmZtime         = nrm2ZmZtime;
results.nrm2Ztime           = nrm2Ztime;


%% results for solution of lyapunov equation
results.Zabs2res            = Zabs2res;
results.Zrel2res            = Zrel2res;
results.ZnrmRHS             = ZnrmRHS;

%% save results to mat file
hostname                    = getComputerName();
timenow                     = datestr(now);
git_id                      = mess_git_id();
git_branch                  = mess_git_branch();

%% delete cluster because it cannot stored to matfiles, then save matfile
z_projection_conf.cluster = [];
z_proj.cluster            = [];

save(mymat,'tspan','ode_solver','ode_opt','hostname','timenow','results','git_id','git_branch');

%% save results to text file
dlmwrite(mydat,z_projection_conf.name, 'delimiter','');
dlmwrite(mydat,timenow, 'delimiter','');
dlmwrite(mydat,hostname,'delimiter','','-append');
dlmwrite(mydat,ode_solver,'delimiter','','-append');
dlmwrite(mydat,sprintf('mess_git_id      = %s', git_id),   'delimiter','','-append');
dlmwrite(mydat,sprintf('mess_git_branch  = %s', git_branch),   'delimiter','','-append');
dlmwrite(mydat,sprintf('Integration time = %f',results.time_integration),   'delimiter','','-append');
dlmwrite(mydat,sprintf('Zabs2res         = %e',results.Zabs2res),           'delimiter','','-append');
dlmwrite(mydat,sprintf('Zrel2res         = %e',results.Zrel2res),           'delimiter','','-append');
dlmwrite(mydat,sprintf('ZnrmRHS          = %e',results.ZnrmRHS),            'delimiter','','-append');
dlmwrite(mydat,sprintf('Zrows            = %d',size(z_proj.Zinf,1)),        'delimiter','','-append');
dlmwrite(mydat,sprintf('Zcols            = %d',size(z_proj.Zinf,2)),        'delimiter','','-append');
dlmwrite(mydat,sprintf('maxabsnrm2err    = %e',results.maxabsnrm2err),      'delimiter','','-append');
dlmwrite(mydat,sprintf('meanabsnrm2err   = %e',results.meanabsnrm2err),     'delimiter','','-append');
dlmwrite(mydat,sprintf('maxrelnrm2err    = %e',results.maxrelnrm2err),      'delimiter','','-append');
dlmwrite(mydat,sprintf('meanrelnrm2err   = %e',results.meanrelnrm2err),     'delimiter','','-append');
dlmwrite(mydat,'Time, ABSNRM2ERR, RELNRM2ERR, NRM2XTRUE, NRM2ZmZtime, NRM2Ztime','delimiter','','-append');
dlmwrite(mydat,[results.tspan', results.absnrm2err, results.relnrm2err, ...
    results.nrm2Xtrue, results.nrm2ZmZtime,results.nrm2Ztime], ...
    'delimiter',',','-append','precision',16);

%% turn diary off
diary('off');


end








