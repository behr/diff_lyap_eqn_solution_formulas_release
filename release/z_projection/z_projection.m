classdef z_projection < handle
    %% Ansatz z_projection constant plus evoluiont for Differential Lyapunov Equation (DLE)
    %
    %  (MESS_OP_NONE)       Edot{X}E' = AXE' +EXA' + BB', X(0)=0
    %
    %  or
    %
    %  (MESS_OP_TRANSPOSE)  E'dot{X}E = A'XE +E'XA + B'B, X(0)=0
    %
    %  This class uses mex-mess to solve the algebraic Lyapunov Equation (ALE)
    %
    %  (MESS_OP_NONE)       0 = AZ_{\infty}Z_{\infy}'E' + EZ_{\infty}Z_{\infty}'A' + BB'
    %
    %  or
    %
    %  (MESS_OP_TRANSPOSE)  0 = A'Z_{\infty}Z_{\infty}'E + E'Z_{\infty}Z_{\infty}'A + B'B
    %
    %  The Solution is given by X(t) = Z_{\infty}*Z_{\infty}' - Z(t)*Zt'.
    %  and Zt solves
    %
    %  (MESS_OP_NONE)       E \dot{Z(t)} = A Z(t), Z(0)=Z_{\infty}
    %
    %  or
    %
    %  (MESS_OP_TRANSPOSE)  E'\dot{Z(t)} = A' Z(t), Z(0)=Z_{\infty}
    %
    %  Since
    %
    %  (MESS_OP_NONE)           Z(t) = e^(tE^(-1)A)Z_{\infty}
    %
    %  or
    %
    %  (MESS_OP_TRANSPOSE)      Z(t) = e^(tE^(-T)A^T)Z_{\infty}
    %
    %  and the columns  of Z_{\infty} contain a invariant subspace for
    %
    %  (MESS_OP_NONE)           E^(-1)A
    %
    %  or
    %
    %  (MESS_OP_TRANSPOSE)      E^(-T)A^T
    %
    %  we can write Z(t) = Q_{\infty} z(t) with Z_{\infty} = Q_{\infty} R_{\infty}.
    %  This leads to the ode for z(t)
    %
    %  (MESS_OP_NONE)           E Q_{\infty}\dot{z}(t) = A Q_{\infty}
    %
    %                               z(0) = Q_{\infty}'Z_{\infty} = R_{\infty}
    %   or
    %
    %  (MESS_OP_TRANSPOSE)      E'Q_{\infty} \dot{z}(t) = A' Q_{\infty} z(t)
    %
    %                               z(0) = Q_{\infty}'Z_{\infty} = R_{\infty}
    %
    %
    %  Inputs:
    %
    %    E                       - (Input) Matrix E of DLE or []
    %    A                       - (Input) Matrix A of DLE
    %    B                       - (Input) Matrix B of DLE
    %    mess_opt                - (Input) mess_options instance
    %    tspan                   - (Input) desired times for solution, rowvector
    %    ode_solver              - (Inout) ode_solver
    %    ode_opt                 - (Input) options for ode solver
    %    cluster                 - (Input) [] if no parallelism wanted, otherwise object from parcluster command
    %    generalized_fourier     - (Input) true/false use generalized fourier (projected mass matrix) or not
    %
    % Maximilian Behr - MPI Magdeburg

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               20016-2018
    %

    properties

        E                       % (Input) Matrix E of DLE or []
        A                       % (Input) Matrix A of DLE
        B                       % (Input) Matrix B of DLE
        mess_opt                % (Input) mess_options instance
        tspan                   % (Input) desired times for solution, rowvector
        ode_solver              % (Inout) ode_solver
        ode_opt                 % (Input) options for ode solver
        cluster                 % (Input) [] if no parallelism wanted, otherwise object from parcluster command
        generalized_fourier     % (Input) true/false use generalized fourier or not


        mess_eqn                % equation object from mex-mess
        mess_stat               % status structure for solution of Lyapunov Equation  0 = A*X*E' + E*X*A' + B*B'
        Zinf                    % Low Solution Factor (MESS_OP_NONE) AXE' + EXA' + BB' / (MESS_OP_TRANSPOSE) A'XE + E'XA + B'B
        Qinf                    % [Qinf,Sinf,~]  = svd(Zinf,0)
        Sinf                    % singular values of truncated Zinf with eps as threshold
        Afourier                % Qinf' * A * Qinf, Qinf' *E^(-1) *A Qinf
        Efourier                % Qinf' * E * Qinf

        z0                      % initial data for the small system
        zt                      % 3D Array contains solution of time variant problem

    end

    methods

        % constructor
        function obj = z_projection(E,A,B,mess_opt,ode_solver,ode_opt,tspan,cluster,generalized_fourier)

            assert(islogical(generalized_fourier),'generalized_fourier is not logical');

            obj.E                       = E;
            obj.A                       = A;
            obj.B                       = full(B);
            obj.tspan                   = tspan;
            obj.mess_opt                = mess_opt;
            obj.ode_solver              = ode_solver;
            obj.ode_opt                 = ode_opt;
            obj.cluster                 = cluster;
            obj.generalized_fourier     = generalized_fourier;

            obj.mess_eqn                = [];
            obj.mess_stat               = [];
            obj.Zinf                    = [];
            obj.Qinf                    = [];
            obj.Sinf                    = [];
            obj.Afourier                = [];
            obj.Efourier                = [];
            obj.z0                      = [];
            obj.zt                      = [];

        end


        function solve_lyap(obj)
            obj.mess_eqn                = mess_equation_glyap(obj.mess_opt,obj.A,obj.E,obj.B);
            [obj.Zinf, obj.mess_stat]   = mess_lradi(obj.mess_eqn,obj.mess_opt);
        end

        function svd_Zinf(obj)
            %% svd_Zinf computes the economy svd of Zinf
            % and therefore an orthnormal bases of the colspace of Zinf
            % we truncate Zinf such that the relative 2-Norm error
            % is small than machine epsilon. If parallelmode is on
            % we mix the columns of Qinf and Zinf, this is a trick to
            % distribute the amount of work for the workers equally.
            colsZold    = size(obj.Zinf,2);
            [Q,S,~]     = svd(obj.Zinf,0);

            %% truncate such the relative 2-Norm error is smaller than eps
            tol         = eps*max(diag(S));
            idx         = diag(S) >= tol;
            Q           = Q(:,idx);
            S           = S(idx,idx);
            obj.Qinf    = Q;
            obj.Sinf    = S;
            obj.Zinf    = obj.Qinf*obj.Sinf;
            fprintf('Truncated cols of Zinf from %d -> %d with relative 2-Norm tolerance = %e.\n',colsZold,size(obj.Zinf,2),tol);
        end



        function compute_fourier_system(obj)
            %% compute reduced matrix
            if isempty(obj.E)
                if obj.mess_opt.type == mess_operation_t.MESS_OP_NONE
                    obj.Afourier = obj.Qinf'*(obj.A*obj.Qinf);
                else
                    obj.Afourier = obj.Qinf'*(obj.A'*obj.Qinf);
                end
            else
                if obj.generalized_fourier
                    % mass in fourier system
                    if obj.mess_opt.type == mess_operation_t.MESS_OP_NONE
                        obj.Afourier = obj.Qinf'*obj.A*obj.Qinf;
                        obj.Efourier = obj.Qinf'*obj.E*obj.Qinf;
                    else
                        obj.Afourier = obj.Qinf'*obj.A'*obj.Qinf;
                        obj.Efourier = obj.Qinf'*obj.E'*obj.Qinf;
                    end
                else
                    % no mass in fourier system
                    if obj.mess_opt.type == mess_operation_t.MESS_OP_NONE
                        obj.Afourier = obj.Qinf'*(obj.E\(obj.A*obj.Qinf));
                    else
                        obj.Afourier = obj.Qinf'*(obj.E'\(obj.A'*obj.Qinf));
                    end
                end
            end

            %% compute initial condition
            obj.z0  = obj.Qinf'*obj.Zinf;

        end

        function solve_fourier_system(obj)

            tspan_              = obj.tspan;
            ode_solver_         = obj.ode_solver;
            ode_options_        = obj.ode_opt;
            tn                  = length(obj.tspan);
            [rowsz0, colsz0]    = size(obj.z0);
            Afourier_           = obj.Afourier;
            Efourier_           = obj.Efourier;
            ode_rhs_            = @(t,x) Afourier_*x;
            pad                 = floor(log10(abs(colsz0)+1))+1; % only for printing

            %% add ode options
            if ~isempty(Efourier_)
                ode_options_ = odeset(ode_options_,'Mass', Efourier_, 'MassSingular','no','MStateDependence','none');
            end
            ode_options_  = odeset(ode_options_,'Jacobian',Afourier_,'JPattern',logical(Afourier_),'BDF','on');

            fprintf('Integrate ODE of Size %d / %d with %d timesteps from t0=%f to t=%f\n',rowsz0,colsz0,length(tspan_),tspan_(1),tspan_(end));
            if isempty(obj.cluster)

                %% allocate memory, swap dimension due to matlabs ode solver stupid return
                obj.zt(tn,rowsz0,colsz0) = 0;

                %% solve for each column
                for j = 1:colsz0
                    fprintf(sprintf('Integrate Col. %%0%1$dd of %%0%1$dd\n',pad),j,colsz0);
                    [~,obj.zt(:,:,j)] = call_ode_solver(ode_solver_, ode_rhs_ , tspan_, obj.z0(:,j), ode_options_);
                end

                %% permute first two dimensions back
                obj.zt = permute(obj.zt,[2 3 1]);

            else

                %% open parallel pool
                pool_open(obj.cluster);

                %% spmd variant
                % Attention, we assume distribution in last dimension
                z0di  = distributed(obj.z0);
                zt_   = Composite();

                spmd
                    %% start timing
                    tstart      = tic;

                    %% get local part of Zinfdi
                    z0loc       = getLocalPart(z0di);

                    %% get local number of cols
                    ncolsloc    = size(z0loc,2);

                    %% get global indices of cols
                    codist      = getCodistributor(z0di);
                    [colsglo]   = codist.globalIndices(2);

                    %% allocate memory on worker and integrate
                    ztloc       = zeros(tn,rowsz0,ncolsloc);
                    for j = 1:ncolsloc
                        fprintf(sprintf('Integrate local/global Col. %%0%1$dd / %%0%1$dd of %%0%1$dd / %%0%1$dd\n',pad),...
                            j,ncolsloc,colsglo(j),colsglo(ncolsloc));
                        [~,ztloc(:,:,j)] = call_ode_solver(ode_solver_, ode_rhs_ , tspan_, z0loc(:,j), ode_options_);
                    end

                    %% populate results
                    zt_  = permute(ztloc,[2 3 1]);
                    fprintf(sprintf('%%s Finished Integrated %%0%1$dd Cols in %%f sec.\n',pad),datestr(now),ncolsloc,toc(tstart));
                end

                %% fetch results to matrix
                obj.zt  = cat(2,zt_{:});

                %% pool close
                pool_close();

            end
        end

        function [Zinf,Zt] = get_dlyap_solution(obj,j)
            %% get solution of dlyap for tspan(j) as Zinf*Zinf' -Zt*Zt'
            assert(0<= j && j<= length(obj.tspan),'j is not a valid index for tspan');

            %% solution given via constant plus projected evolution
            Zinf = obj.Zinf;
            Zt   = obj.Qinf*obj.zt(:,:,j);

        end

        function [absres2,relres2,nrmRHS] =  res2_lyap(obj)
            %% compute the relative and absolute 2-Norm residual of Lyapunov Equation

            E_ = obj.E;
            A_ = obj.A;
            B_ = obj.B;
            Z_ = obj.Zinf;

            absres2 = res2_lyap(A_,E_,B_,Z_,obj.mess_opt.type);
            nrmRHS = norm(B_)^2;
            relres2 = absres2/nrmRHS;

        end


        function [nrm2err, nrm2Xtrue, nrm2ZmZtime, nrm2Ztime] = compute_error(obj,get_true_solution)

            ltspan      = length(obj.tspan);
            pad         = floor(log10(abs(ltspan)+1))+1;   % for printing


            if isempty(obj.cluster)

                % sequentiell norm computation
                nrm2err     = zeros(ltspan,1);
                nrm2Xtrue   = zeros(ltspan,1);
                nrm2ZmZtime = zeros(ltspan,1);
                nrm2Ztime   = zeros(ltspan,1);

                for j = 1:ltspan
                    fprintf(sprintf('Compute Error for iterate %%0%1$dd of %%0%1$dd at tspan(%%0%1$dd)=%%f\n',pad),j,ltspan,j,obj.tspan(j));
                    Xtrue           = get_true_solution(obj.tspan(j));
                    [Zinf_,Zt]      = get_dlyap_solution(obj,j);
                    nrm2err(j)      = residual_dle(Zinf_, Zt, Xtrue);
                    %nrm2Xtrue(j)    = norm(Xtrue,2);
                    nrm2Xtrue(j)    = max(abs(eigs(Xtrue, 1, 'LM')));    % Xtrue is symmetric
                    nrm2ZmZtime(j)  = max(abs(eig([Zinf_';Zt']*[Zinf_,-Zt]))); % || Z1 * Z1' - Z2 * Z2'||_2
                    %nrm2ZmZtime(j)  = max(abs(eigs(@(x) Zinf_*(Z_inf'*x) - Zt*(Zt'*x), size(Zinf_,1), 1, 'LM')));
                    nrm2Ztime(j)    = norm(Zt)^2;
                end

            else

                %% open parallel pool
                pool_open(obj.cluster);

                % parallel norm computation
                ztdist          = distributed(obj.zt);
                Zinf_           = obj.Zinf;
                Qinf_           = obj.Qinf;
                tspan_          = obj.tspan;
                nrm2err         = Composite();
                nrm2Xtrue       = Composite();
                nrm2ZmZtime     = Composite();
                nrm2Ztime       = Composite();

                spmd
                    tstart = tic;

                    % get local part of Ztimedist
                    ztdistloc       = getLocalPart(ztdist);

                    % get local of dim 3
                    ndim3loc        = size(ztdistloc,3);

                    % get global of dim 3
                    codist          = getCodistributor(ztdist);
                    [ndim3glo]      = codist.globalIndices(3);

                    % allocate
                    nrm2errloc      = zeros(ndim3loc,1);
                    nrm2Xtrueloc    = zeros(ndim3loc,1);
                    nrm2ZmZtimeloc  = zeros(ndim3loc,1);
                    nrm2Ztimeloc    = zeros(ndim3loc,1);

                    for j = 1:ndim3loc
                        fprintf(sprintf('Compute Error for iterate loc./glo. j = %%0%1$dd / %%0%1$dd of %%0%1$dd / %%0%1$dd at tspan(%%0%1$dd)=%%f\n',pad), ...
                            j,ndim3loc(end),ndim3glo(j),ltspan,ndim3glo(j),tspan_(ndim3glo(j)));
                        Xtrue               = get_true_solution(tspan_(ndim3glo(j)));
                        Zt                  = Qinf_*ztdistloc(:,:,j);
                        nrm2errloc(j)       = residual_dle(Zinf_, Zt,Xtrue);
                        %nrm2Xtrueloc(j)     = norm(Xtrue,2);
                        nrm2Xtrueloc(j)     = max(abs(eigs(Xtrue, 1, 'LM')));    % Xtrue is symmetric

                        nrm2ZmZtimeloc(j)   = max(abs(eig([Zinf_';Zt']*[Zinf_,-Zt]))); % || Z1 * Z1' - Z2 * Z2'||_2
                        %nrm2Ztimeloc(j)     = norm(Zt'*Zt);
                        %nrm2ZmZtimeloc(j)   = max(abs(eigs(@(x) Zinf_*(Z_inf'*x) - Zt*(Zt'*x), size(Zinf_,1), 1, 'LM')));
                        nrm2Ztimeloc(j)     = norm(Zt)^2;
                    end

                    % populate results
                    nrm2err                 = nrm2errloc;
                    nrm2Xtrue               = nrm2Xtrueloc;
                    nrm2ZmZtime             = nrm2ZmZtimeloc;
                    nrm2Ztime               = nrm2Ztimeloc;

                    fprintf(sprintf('%%s Finished Norm Computation of %%0%1$dd iterates in %%f sec.\n',pad),datestr(now),ndim3loc,toc(tstart));

                end

                %% fetch results from workers
                nrm2err         = cat(1,nrm2err{:});
                nrm2Xtrue       = cat(1,nrm2Xtrue{:});
                nrm2ZmZtime     = cat(1,nrm2ZmZtime{:});
                nrm2Ztime       = cat(1,nrm2Ztime{:});

                pool_close()

            end

        end

    end


end


