% This Scripts loads the Rail data print the norm of the rhs matrices
% B and C.
% Maximilian Behr, MPI Magdeburg

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20016-2018
%


%% clear all
clear all, close all, clc

%% load data for Rail, and print norm of rhs
railversions = {'rail371.mat', 'rail1357.mat', 'rail5177.mat', 'rail20209.mat', 'rail79841.mat'};
%railversions = {'rail371.mat', 'rail1357.mat'};

for railversion  = railversions
    rail                        = load(railversion{:});
    fprintf('%s, n = %d  || B ||_2  = %e\n',datestr(now),size(rail.A,1),norm(full(rail.B)));
    fprintf('%s, n = %d  || C ||_2  = %e\n',datestr(now),size(rail.A,1),norm(full(rail.C)));
end

