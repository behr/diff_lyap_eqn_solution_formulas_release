%% a simple script which plots results

%% load all mat files from results folder
result_mats=dir(fullfile(fileparts(mfilename('fullpath')),'results/**/*.mat'));

%% plot absolute and relative two norm error
for i = 1:length(result_mats)
    matfile = fullfile(result_mats(i).folder,result_mats(i).name);
    [matfilepath, matfilename, ~] =  fileparts(matfile);
    mat = load(matfile);
    
    %% plot absolute and relative error and write to disk
    fprintf('Mat Name       = %s\n',matfile);
    fprintf('ODE-Solver     = %s\n',mat.ode_solver);
    fprintf('maxabsnrm2err  = %e\n',mat.results.maxabsnrm2err);
    fprintf('maxrelnrm2err  = %e\n',mat.results.maxrelnrm2err);
    fprintf('meanrelnrm2err = %e\n',mat.results.meanrelnrm2err);
    fprintf('\n')
    
end
