classdef test_z_projection < matlab.unittest.TestCase
    % test_z_projection class is a matlab unittest.
    % The function of z_projection_error is tested
    % with rail matrices.
    %
    % Maximilian Behr, MPI-Magdeburg
    %
    %

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               20016-2018
    %

    properties
        rails           = {'rail371.mat', 'rail1357.mat'};
        ode_solver      = 'ode15s';
        ops             = {mess_operation_t.MESS_OP_NONE, mess_operation_t.MESS_OP_TRANSPOSE};
        tspan           = linspace(0,10,100);
        verbose         = true;
        tol             = 1e-1;
    end

    methods(Test)
        function test_lyap_rail(test)

            %% iterate over sizes
            for rail_idx = length(test.rails)

                %% iterate over operation types
                for op_idx = length(test.ops)


                    %% get rail and operation type
                    rail = load(test.rails{rail_idx});
                    op = test.ops{op_idx};

                    if op == mess_operation_t.MESS_OP_NONE
                       op_name = 'MESS_OP_NONE';
                    else
                       op_name = 'MESS_OP_TRANSPOSE';
                    end

                    %% create config
                    conf = z_projection_conf();
                    conf.name           = sprintf('test_%s_rail_%s_%s_sequentiell',class(test),test.rails{rail_idx},op_name);
                    conf.ode_solver     = test.ode_solver;
                    conf.mess_opt.type  = op;
                    conf.tspan          = test.tspan;
                    conf.cluster        = []; %parcluster('local');

                    %% create solver from spectral decomposition
                    if conf.mess_opt.type == mess_operation_t.MESS_OP_NONE
                        lyap_spectral = lyap_spectral_solver(rail.E, rail.A, rail.B, true);
                    else
                        lyap_spectral = lyap_spectral_solver(rail.E', rail.A', rail.C', true);
                    end

                    %% solve in parallel and sequential and get error
                    if conf.mess_opt.type == mess_operation_t.MESS_OP_NONE
                        results_sequentiell =  z_projection_error(rail.E,rail.A,rail.B,conf,lyap_spectral);
                        conf.cluster        = parcluster('local');
                        conf.name           = sprintf('test_%s_rail_%s_%s_parallel_local',class(test),test.rails{rail_idx},op_name);
                        results_parallel =  z_projection_error(rail.E,rail.A,rail.B,conf,lyap_spectral);
                    else
                        results_sequentiell = z_projection_error(rail.E,rail.A,rail.C,conf,lyap_spectral);
                        conf.cluster        = parcluster('local');
                        conf.name           = sprintf('test_%s_rail_%s_%s_parallel_local',class(test),test.rails{rail_idx},op_name);
                        results_parallel    = z_projection_error(rail.E,rail.A,rail.C,conf,lyap_spectral);
                    end

                    %% print some information and compare results
                    if test.verbose
                        fprintf('Rail          = %s\n', test.rails{rail_idx});
                        fprintf('OP            = %s\n', op_name);
                        fprintf('ode_solver    = %s\n', test.ode_solver);

                        fprintf('Parallel\n');
                        fprintf('Time Integration   = %e\n',results_parallel.time_integration);
                        fprintf('maxrelnrm2err      = %e\n',results_parallel.maxrelnrm2err);
                        fprintf('meanrelnrm2err     = %e\n',results_parallel.meanrelnrm2err);
                        fprintf('maxabsnrm2err      = %e\n',results_parallel.maxabsnrm2err);
                        fprintf('meanabsnrm2err     = %e\n',results_parallel.meanabsnrm2err);

                        fprintf('Sequentiell\n');
                        fprintf('Time Integration   = %e\n',results_sequentiell.time_integration);
                        fprintf('maxrelnrm2err      = %e\n',results_sequentiell.maxrelnrm2err);
                        fprintf('meanrelnrm2err     = %e\n',results_sequentiell.meanrelnrm2err);
                        fprintf('maxabsnrm2err      = %e\n',results_sequentiell.maxabsnrm2err);
                        fprintf('meanabsnrm2err     = %e\n',results_sequentiell.meanabsnrm2err);
                        fprintf('\n\n');
                    end

                    %% compare sequentiell and parallel results
                    test.fatalAssertLessThan(abs(results_sequentiell.maxrelnrm2err  - results_parallel.maxrelnrm2err), test.tol);
                    test.fatalAssertLessThan(abs(results_sequentiell.meanrelnrm2err - results_parallel.meanrelnrm2err), test.tol);

                    test.fatalAssertLessThan(abs(results_sequentiell.meanabsnrm2err - results_parallel.meanabsnrm2err), test.tol);
                    test.fatalAssertLessThan(abs(results_sequentiell.maxabsnrm2err  - results_parallel.maxabsnrm2err), test.tol);

                end
            end

        end

    end
end

