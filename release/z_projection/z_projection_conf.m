classdef z_projection_conf < handle
    %% z_proj_conf holds all options
    % in default setting which are necessay for z projection.
    % A standard configuration is returned by the solver.
    %
    % Maximilian Behr - MPI Magdeburg

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               20016-2018
    %

    properties

        name                    % default: 'empty', name of your example
        cluster                 % default: []
        tspan                   % default: unique([linspace(0,30,100),linspace(30,4500,100)]);
        ode_solver              % default: 'ode_23tb'
        ode_opt                 % default: see code
        mess_opt                % default: see code
        generalized_fourier     % true
    end

    methods

        % constructor
        function obj = z_projection_conf()
            obj.name                            = 'empty';
            obj.cluster                         = [];
            obj.tspan                           = unique([linspace(0,30,100),linspace(30,4500,100)]);
            obj.ode_solver                      = 'ode_23tb';
            obj.ode_opt                         = odeset('RelTol',1e-9,'AbsTol',1e-10,'Stats','off','NormControl','off');
            obj.mess_opt                        = mess_options();
            obj.mess_opt.type                   = mess_operation_t.MESS_OP_NONE;
            obj.mess_opt.adi.res2_tol           = 1e-11;
            obj.mess_opt.adi.res2c_tol          = 1e-12;
            obj.mess_opt.adi.rel_change_tol     = 1e-10;
            obj.generalized_fourier             = true;
        end

    end

end
