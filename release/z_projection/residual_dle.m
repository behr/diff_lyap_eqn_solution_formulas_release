function res2 = residual_dle(Zinf,Zt,Xtrue)
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20016-2018
%

res2 = abs(eigs(@(x) Zinf*(Zinf'*x) - Zt*(Zt'*x) - Xtrue*x,size(Zinf,1),1,'LM'));

end
