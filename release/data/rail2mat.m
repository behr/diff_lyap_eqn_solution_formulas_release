% Read all Rail Matrices from mtx file and write to mat binary files.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20017-2018
%

%% clear all
clear all, close all, clc

%% read rail matrices and store
railmat = @(n,name) sprintf('rail_%d_c60.%s',n,name);

for n = [371, 1357, 5177, 20209, 79841]
    A = mmread(which(railmat(n,'A')));
    E = mmread(which(railmat(n,'E')));
    B = mmread(which(railmat(n,'B')));
    C = mmread(which(railmat(n,'C')));
    B = full(B);
    C = full(C);
    
    % create filename for mat file
    [raildir, railname, railext]  = fileparts(which(railmat(n,'A')));
    railmatname                   = fullfile(raildir,sprintf('rail%d.mat',n));
    save(railmatname,'A','E','B','C');
end


