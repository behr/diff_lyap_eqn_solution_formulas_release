function rail_eigensystem(E,A,matname)
%% RAIL_EIGENSYSTEM solves the eigenvalue problems
%
%  [VA,DA]      = eig(A);
%  [VE,DE]      = eig(E);
%  [VAE,DAE]    = eig(A,E);
%
%  We compute also the following residuals
%
%  VA_abs2nrmerr    = norm(A*VA-VA*DA);
%  VA_orth2nrmerr   = norm(VA'*VA-eye(size(VA)))
%
%  VE_abs2nrmerr    = norm(E*VE-VE*DE);
%  VE_orth2nrmerr   = norm(VE'*VE-eye(size(VE)))
%
%  VAE_abs2nrmerr   = norm(A*VAE-E*VAE*DAE);
%  VAE_orth2nrmerr  = norm(VAE'*E*VAE-eye(size(VAE)))
%
%  The eigendecompositions and the residuals are stored in a mat file
%  with name matname (absolute path).
%
%
% Maximilian Behr, MPI-Magdeburg

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20016-2018
%


%% compute eigendecompositions
A = full(A); E = full(E);
[VA,DA]      = eig(A);
[VE,DE]      = eig(E);
[VAE,DAE]    = eig(A,E);


%% compute residuals
VA_abs2nrmerr    = norm(A*VA-VA*DA);
VA_orth2nrmerr   = norm(VA'*VA-eye(size(VA)));

VE_abs2nrmerr    = norm(E*VE-VE*DE);
VE_orth2nrmerr   = norm(VE'*VE-eye(size(VE)));

VAE_abs2nrmerr   = norm(A*VAE-E*VAE*DAE);
VAE_orth2nrmerr  = norm(VAE'*E*VAE-eye(size(VAE)));



%% save objects
save(matname,'VA','DA','VE','DE','VAE','DAE',...
             'VA_abs2nrmerr','VA_orth2nrmerr',...
             'VE_abs2nrmerr','VE_orth2nrmerr',...
             'VAE_abs2nrmerr','VAE_orth2nrmerr',...
             '-v7.3');

end
