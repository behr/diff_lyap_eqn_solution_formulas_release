% This Scripts loads the Rail data and computes the spectral decomposition
% of A, E and (A,E)
% The results are stored in an mat file.
% For further informations see solve_eigensystem_rail.m
%
% Maximilian Behr, MPI Magdeburg

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20016-2018
%


%% clear all
clear all, close all, clc

%% load data for Rail, solve Lyapunov Equation and Store Data
%railversions = {'rail371.mat', 'rail1357.mat', 'rail5177.mat', 'rail20209.mat', 'rail79841.mat'};
%railversions = {'rail371.mat','rail1357.mat','rail5177.mat','rail20209.mat'};
%railversions = {'rail371.mat'};
railversions = {'rail371.mat','rail1357.mat'};
%railversions = {'rail371.mat','rail1357.mat','rail5177.mat'};
for railversion  = railversions

    [raildir,railname,railext]  = fileparts(which(railversion{:}));
    railmatname                 = fullfile(raildir,strcat(railname,'_spectral_decomp.mat'));
    rail                        = load(railversion{:});
    fprintf('%s, Start Computing Eigendecomposition of Rail %s\n',datestr(now),railname);
    rail_eigensystem(rail.E,rail.A,railmatname);
    fprintf('%s, Finished Computing Eigendecomposition of Rail %s\n',datestr(now),railname);
    send_mail_system_mail(sprintf('Finished for Rail %s',railname),'','behr@mpi-magdeburg.mpg.de');
end

