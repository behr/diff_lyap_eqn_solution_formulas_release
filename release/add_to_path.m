% Add all required directories to the MATLAB path.
% Run this script to add all required functions and directories to the.
% MATLAB path in order to run MEX-M.E.S.S. functions and demos.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20016-2018
%

%% add projection based solver code
s=pwd;
addpath(s);
addpath(genpath(sprintf('%s/data',s)));
addpath(genpath(sprintf('%s/mmread',s)));
addpath(genpath(sprintf('%s/helpers',s)));
addpath(genpath(sprintf('%s/lyap_spectral_solver',s)));
addpath(genpath(sprintf('%s/solve_eigensystem_rail',s)));
addpath(genpath(sprintf('%s/z_projection',s)));

%% add mex-mess
addpath(genpath(sprintf('%s/mess_build/matlab',s)));
mess_path

%% add ldlt solver
s=pwd;
addpath(genpath(sprintf('%s/ldlt',s)));
fprintf('Start Downloading LDL^T Code from https://zenodo.org/record/834953/files/LTV_BT.zip?download=1\n');
cd(fullfile(s,'ldlt'));
unzip('https://zenodo.org/record/834953/files/LTV_BT.zip?download=1')
cd('..');
fprintf('Add LDL^T Code to path\n'); 
addpath(genpath(sprintf('%s/ldlt/LTV_BT',s)));

%% clear
clear s;
