% LYAP_SPECTRAL_SOLVER Class implements a spectral decomposition based solver for
%
%  Algebraic Lyapunov Equations (ALE):
%
%  0 = AXE' + EXA'   + BB'
%  0 = AX   +  XA'   + BB'
%
%  and Differential Lyapunov Equations (DLE):
%
%  Edot{X}E' = AXE' + EXA'   + BB', X(0)=0
%   dot{X}   = AX   +  XA'   + BB', X(0)=0
%
%  The class computes the generalized eigenvalue decomposition
%  of the matrix Pair (A,E) or the eigenvalue decomposition of A
%  if E=[]. The (generalized) eigenvalue decomposition
%  is used to compute the solution of the ALE(s)
%  and DLE(s) above.
%
%   LYAP_SPECTRAL_SOLVER properties:
%       E               - Matrix E of ALE/DLE
%       A               - Matrix A of ALE/DLE
%       B               - Matrix B of ALE/DLE
%       isorth          - Boolean, true if (generalized) Eigenvectors are orthogonal, V'*V = I_n or V*E*V'=I_n
%       D               - (generalized) Eigenvalues of (A,E)
%       V               - (generalized) right Eigenvectors of (A,E)
%       Coeff           - Coeff(i,j) = eigenvalue(i)+conj(eigenvalue(j)) of (A,E)
%       VinvB2          - (V\B)*(V\B)' or (V\E\B)*(V\E\B)'
%       realdata        - true or false if all data are real
%
%   LYAP_SPECTRAL_SOLVER methods:
%       lyap_spectral_solver                        - Constructor, generate a solver from Data E, A, B.
%       solve_lyap                                  - Solve Lyapunov equation using spectral decompostion.
%       solve_dlyap                                 - Solve  Differential Lyapunov equation using spectral decompostion.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20017-2018
%

classdef lyap_spectral_solver < handle
    
    properties
        
        E               % (Input) Matrix E of ALE/DLE
        A               % (Input) Matrix A of ALE/DLE
        B               % (Input) Matrix B of ALE/DLE
        isorth          % (Input) specify that the (generalized) Eigenvectors are orthogonal, V'*V = I_n or V*E*V'=I_n
        
        D               % generalized Eigenvalues of        (A,E)
        V               % generalized right Eigenvectors of (A,E)
        Coeff           % Coeff(i,j) = eigenvalue(i)+conj(eigenvalue(j)) of (A,E)
        VinvB2          % (V\B)*(V\B)' or (V\E\B)*(V\E\B)'
        realdata        % true or false if all data are real
        
    end
    
    methods
        
        % constructor
        function obj = lyap_spectral_solver(E,A,B,isorth, V, D)
            %% init realdata and isorth flag
            obj.realdata = true;
            assert(islogical(isorth),'Wrong Input Arguments');
            obj.isorth = isorth;
            
            
            %% check matrix E
            if ~isempty(E)
                assert(ismatrix(E),'%s is no matrix.',inputname(1));
                assert(size(E,1)==size(E,2),'%s is not square.',inputname(1));
                obj.realdata = obj.realdata && isreal(E);
            end
            
            %% check matrix A
            assert(ismatrix(A),'%s is no matrix.',inputname(2));
            assert(size(A,1)==size(A,2),'%s is not square.',inputname(2));
            obj.realdata = obj.realdata && isreal(A);
            
            %% check matrix B
            assert(ismatrix(B),'%s is no matrix.',inputname(3));
            assert(size(A,1)==size(B,1),'Matrix %s does not fit to Matrix %s.',inputname(2),inputname(3));
            assert(size(B,1)>=size(B,2),'Matrix %s is oversized (no low rank).',inputname(3));
            obj.realdata = obj.realdata && isreal(B);
            
            %% set matrices
            obj.E = E;
            obj.A = A;
            obj.B = full(B);
            
            %% compute (generalized) eigendecomposition
            if nargin == 4
                if isempty(obj.E)
                    fprintf('%s Compute Eigendecomposition of size %d-x-%d.\n',datestr(now),size(A,1),size(A,2));
                    [obj.V,obj.D]   = eig(full(obj.A));
                else
                    fprintf('%s Compute generalized Eigendecomposition of size %d-x-%d.\n',datestr(now),size(A,1),size(A,2));
                    [obj.V,obj.D]   = eig(full(obj.A),full(obj.E));
                end
                obj.D = diag(obj.D);
            elseif nargin == 6
                obj.V = V;
                if isvector(D)
                    obj.D = D;
                else
                    obj.D = diag(D);
                end                
            else
                error('Either the full spectral decomposition is given or not.');
            end
            
            %% set up coefficient matrix Coeff(i,j)=eigenvalue(i)+conj(eigenvalue(j))
            %obj.Coeff = zeros(size(A));
            obj.Coeff(size(A,1),size(A,2))=0;
            for j = 1:size(obj.Coeff,1)
                obj.Coeff(:,j) = obj.D + conj(obj.D(j));
            end
            
            %% ensure that Coeff Matrix is Hermitian
            obj.Coeff = 0.5*(obj.Coeff+obj.Coeff');
            
            %% prepare right hand side matrix (B) for ALE and DLE solver
            if obj.isorth
                %V'V=I -> V' = inv(V) and V'MV=I -> V'M=V^{-1}
                temp = obj.V'*obj.B;
            else
                if ~isempty(obj.E)
                    temp = obj.V\(obj.E\obj.B);
                else
                    temp = obj.V\obj.B;
                end
            end
            
            % ensure symmetry this can be complex because eigenvalues
            % of real matrices can be complex
            obj.VinvB2 = temp*temp';
            obj.VinvB2 = 0.5*(obj.VinvB2+obj.VinvB2');
            
        end
        
        function X = solve_lyap(obj)
            %  Get Solution of ALE, 0 = AXE' + EXA'   + BB' or 0 = AX   +  XA'   + BB'.
            X = -obj.V*(lyap_spectral_solver.inv_variant1(obj.Coeff).*obj.VinvB2)*obj.V';
            if obj.realdata
                X = real(X);
            end
            X = 0.5*(X+X');
        end
        
        function X = solve_dlyap(obj,t)
            %  Get Solution of DLE,
            %  E\dot{X}E' = AXE' + EXA'   + BB', X(0)=0 or
            %   \dot{X}   = AX   +  XA'   + BB', X(0)=0.
            %  at time t.
            assert(isreal(t),'t is not real.');
            X = obj.V*(lyap_spectral_solver.exp_variant1(t,obj.Coeff).*obj.VinvB2)*obj.V';
            if obj.realdata
                X = real(X);
            end
            X = 0.5*(X+X');
        end
        
    end
    
    methods(Static)
        
        function y = inv_variant1(x)
            y = 1./x;
        end
        
        function y = exp_variant1(t,x)
            if t==0
                y = zeros(size(x));
            else
                y = exp(t*x)./x-1./x;
            end
        end
        
    end
    
end


