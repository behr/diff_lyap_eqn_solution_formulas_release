classdef test_lyap_spectral_solver < matlab.unittest.TestCase
    % test_lyap_spectral_solver class is a matlab unittest.
    % The methods of lyap_spectral_solver are tested
    % with small random matrices
    %
    % Maximilian Behr, MPI-Magdeburg

    properties
        tol             = sqrt(eps);        % tolerance for test
        dims            = 5:15;             % dimension of A and E
        havesE          = 0:1;              % matrix E on/off
        colsB           = 1:5;              % columns of Matrix B
        dtsA            = 0:1;              % datatype of test matrix A: 0 Real, 1 Complex
        dtsE            = 0:1;              % datatype of test matrix E: 0 Real, 1 Complex
        dtsB            = 0:1;              % datatype of test matrix B: 0 Real, 1 Complex
        herms           = 0:1;              % make A and B hermitian or not
        output          = false;             % turn output on/off
        tspan           = 0:0.1:4;          % time span for dlyap test
        odesolver       = 'ode113';         % ode solver
        odeopt          = odeset('RelTol',1e-8,'AbsTol',1e-12,'Stats','on','BDF','on'); %options for ode solver
        odetol          = 1e-6;             % tolerance for ode solver
        dimode          = [10,15,20];       % dimension of A and E for ODE test
        colsBode        = 1:5;              % columns for B for Ode test
        railinstance    = 'rail371.mat';    % the test instance for the rial tests
    end

    methods(Test)
        function test_lyap_rail(test)

            %% load matrices
            Rail = load(test.railinstance);

            %% generate lyap spectral solver for generalized and standard lyapunov equation
            lyap_spectral_gen = lyap_spectral_solver(Rail.E,Rail.A,Rail.B,true);
            lyap_spectral_std = lyap_spectral_solver([],Rail.A,Rail.B,true);
            B = Rail.B;
            A = Rail.A;
            E = Rail.E;

            %% solve lyapunov equation and compute residual
            Xgen = solve_lyap(lyap_spectral_gen);
            Xstd = solve_lyap(lyap_spectral_std);

            %% compute error
            nrm2Xgen    = norm(Xgen,2);
            nrm2Xstd    = norm(Xstd,2);
            nrmfXgen    = norm(Xgen,'fro');
            nrmfXstd    = norm(Xstd,'fro');
            nrm2RHS     = norm(B'*B,2);
            nrmfRHS     = norm(B'*B,'fro');

            res2gen = norm(A*Xgen*E' + E*Xgen*A' + B*B',2);
            resfgen = norm(A*Xgen*E' + E*Xgen*A' + B*B','fro');
            res2std = norm(A*Xstd + Xstd*A' + B*B',2);
            resfstd = norm(A*Xstd + Xstd*A' + B*B','fro');


            if test.output
                fprintf(' --- 2-Norm Results ---\n');
                fprintf('||Xstd||2                                 = %e\n',nrm2Xstd);
                fprintf('||Xgen||2                                 = %e\n',nrm2Xgen);
                fprintf('||RHS||2                                  = %e\n',nrm2RHS);
                fprintf('||AX    + XA^T  +  B*B^T||2               = %e\n',res2std);
                fprintf('||AX    + XA^T  +  B*B^T||2 / ||B*B^T||2  = %e\n',res2std/nrm2RHS);
                fprintf('||AXE^T + EXA^T +  B*B^T||2               = %e\n',res2gen);
                fprintf('||AXE^T + EXA^T +  B*B^T||2 / ||B*B^T||2  = %e\n\n',res2gen/nrm2RHS);
                fprintf(' --- Frobenius Norm Results ---\n');
                fprintf('||Xstd||F                                 = %e\n',nrmfXstd);
                fprintf('||Xgen||F                                 = %e\n',nrmfXgen);
                fprintf('||RHS||F                                  = %e\n',nrmfRHS);
                fprintf('||AX    + XA^T  + B*B^T||F                = %e\n',resfstd);
                fprintf('||AX    + XA^T  + B*B^T||F / ||B*B^T||F   = %e\n',resfstd/nrmfRHS);
                fprintf('||AXE^T + EXA^T + B*B^T||F                = %e\n',resfgen);
                fprintf('||AXE^T + EXA^T + B*B^T||F / ||B*B^T||F   = %e\n',resfgen/nrmfRHS);
                fprintf('------------------------------------------------------\n\n');
            end


            %% check results absolute and relative
            % residual
            test.fatalAssertLessThan(res2std,test.tol);
            test.fatalAssertLessThan(res2std/nrm2RHS,test.tol);
            test.fatalAssertLessThan(resfstd,test.tol);
            test.fatalAssertLessThan(resfstd/nrmfRHS,test.tol);

            test.fatalAssertLessThan(res2gen,test.tol);
            test.fatalAssertLessThan(res2gen/nrm2RHS,test.tol);
            test.fatalAssertLessThan(resfgen,test.tol);
            test.fatalAssertLessThan(resfgen/nrmfRHS,test.tol);

        end

        function test_lyap_solver(test)

            for dim = test.dims
                for haveE = test.havesE
                    for colB = test.colsB
                        for dtA = test.dtsA
                            for dtE = test.dtsE
                                for dtB = test.dtsB
                                    for herm = test.herms

                                        %% generate random matrices A, B and RHS
                                        A   = generate_matrix(dim,dim,dtA);
                                        E   = generate_matrix(dim,dim,dtE);
                                        B   = generate_matrix(dim,colB,dtB);

                                        %% set matrix E empty if necessary
                                        if ~haveE
                                            E=[];
                                        end

                                        if herm
                                            A = A+A';
                                            E = E+E';
                                        end

                                        %% generate lyap spectral solver
                                        lyap_spectral = lyap_spectral_solver(E,A,B,false);

                                        %% solve lyapunov equation and compute residual
                                        X = solve_lyap(lyap_spectral);

                                        nrm2X    = norm(X,2);
                                        nrmfX    = norm(X,'fro');
                                        nrm2RHS  = norm(B'*B,2);
                                        nrmfRHS  = norm(B'*B,'fro');

                                        if haveE
                                            res2 = norm(A*X*E' + E*X*A' + B*B',2);
                                            resf = norm(A*X*E' + E*X*A' + B*B','fro');
                                        else
                                            res2 = norm(A*X + X*A' + B*B',2);
                                            resf = norm(A*X + X*A' + B*B','fro');
                                        end


                                        if test.output
                                            fprintf('dim=%d haveE=%d colB=%d isreal(A)=%d isreal(E)=%d isreal(B)=%d\n', ...
                                                dim,haveE,colB,isreal(A),isreal(E),isreal(B));
                                            fprintf(' --- 2-Norm Results ---\n');
                                            fprintf('||X||2                                 = %e\n',nrm2X);
                                            fprintf('||RHS||2                               = %e\n',nrm2RHS);
                                            fprintf('||AX + XA^T + B*B^T||2                 = %e\n',res2);
                                            fprintf('||AX + XA^T + B*B^T||2 / ||B*B^T||2    = %e\n\n',res2/nrm2RHS);
                                            fprintf(' --- Frobenius Norm Results ---\n');
                                            fprintf('||X||F                                 = %e\n',nrmfX);
                                            fprintf('||RHS||F                               = %e\n',nrmfRHS);
                                            fprintf('||AX + XA^T + B*B^T||F                 = %e\n',resf);
                                            fprintf('||AX + XA^T + B*B^T||F / ||B*B^T||F    = %e\n',resf/nrmfRHS);
                                            fprintf('-----------------------------------------------------\n\n');
                                        end

                                        % check results absolute and relative
                                        % residual
                                        test.fatalAssertLessThan(res2,test.tol);
                                        test.fatalAssertLessThan(res2/nrm2RHS,test.tol);
                                        test.fatalAssertLessThan(resf,test.tol);
                                        test.fatalAssertLessThan(resf/nrmfRHS,test.tol);
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end

        function test_dlyap_solver(test)

            for dim = test.dimode

                for colB = test.colsBode
                    for haveE = test.havesE
                        %% generate random matrices real stable A, E and RHS
                        eigmax = -1e-5;
                        eigmin = -1e+3;

                        % loop as long we have a stable matrix pair
                        stop = 1;
                        while stop

                            dA  = eigmin+rand(dim,1)*(eigmin-eigmax);   % negativ definit matrix
                            dE  = 1+rand(dim,1);                        % positiv definite matrix
                            VA  = generate_matrix(dim,dim,0);
                            VE  = generate_matrix(dim,dim,0);
                            A   = VA*diag(dA)/VA;
                            E   = VE*diag(dE)/VE;

                            % test if matrix pair is stable
                            if haveE
                                e = eig(A,E);
                            else
                                e = eig(A);
                            end

                            if all(real(e)<0)
                                stop = 0;
                            else
                                fprintf('Try again to find a stable Matrix Pair for Testing.\n');
                            end
                        end

                        B = generate_matrix(dim,colB,0);

                        %% set matrix E empty if necessary
                        if ~haveE
                            E=[];
                        end

                        %% generate lyap spectral solver
                        lyap_spectral = lyap_spectral_solver(E,A,B,false);

                        %% compute solution for comparison
                        if haveE
                            EinvA = E\A;
                            EinvB = E\B;
                            EinvB2 = EinvB*EinvB';
                            rhsdlyap =@(t,X) vec(EinvA*invvec(X)+invvec(X)*EinvA' + EinvB2);
                            jacobian = kron(eye(size(A)),EinvA) + kron(EinvA,eye(size(A)));
                            myode_opt = odeset(test.odeopt,'Jacobian',jacobian,'JPattern',logical(jacobian));

                            %Mass=kron(conj(E),E);
                            %rhsdlyap =@(t,X) vec(A*invvec(X)+invvec(X)*A' + B*B');
                            %jacobian = kron(eye(size(A)),A) + kron(A,eye(size(A)));
                            %myode_opt = odeset(test.odeopt,'Mass', Mass, 'MassSingular','no','MStateDependence','none', ...
                            %                   'Jacobian',jacobian,'JPattern',logical(jacobian));
                        else
                            B2 = B*B';
                            rhsdlyap =@(t,X) vec(A*invvec(X)+invvec(X)*A' + B2);
                            jacobian = kron(eye(size(A)),A) + kron(A,eye(size(A)));
                            myode_opt = odeset(test.odeopt,'Jacobian',jacobian,'JPattern',logical(jacobian));
                        end


                        %% solve differential lyapunov equation and compare with results from matlab ode solver

                        [~,Xtime]   = call_ode_solver(test.odesolver,rhsdlyap, test.tspan, zeros(dim*dim,1), myode_opt);

                        if test.output
                            fprintf('dim=%d haveE=%d colB=%d isreal(A)=%d isreal(E)=%d isreal(B)=%d\n', ...
                                dim,haveE,colB,isreal(A),isreal(E),isreal(B));
                        end


                        for j = 1:length(test.tspan)
                            Xspectral = solve_dlyap(lyap_spectral,test.tspan(j));
                            Xode      = invvec(Xtime(j,:)');
                            diff2nrm  = norm(Xspectral-Xode);
                            diffFnrm  = norm(Xspectral-Xode,'fro');
                            rel2err   = diff2nrm/norm(Xode);
                            relFerr   = diffFnrm/norm(Xode,'fro');


                            if test.output
                                fprintf('t=%f\n',test.tspan(j));
                                fprintf('||Xspectral-Xode||2                = %e\n',diff2nrm);
                                fprintf('||Xspectral-Xode||2/||Xode||2      = %e\n',rel2err);
                                fprintf('||Xspectral-Xode||F                = %e\n',diffFnrm);
                                fprintf('||Xspectral-Xode||F/||Xode||F      = %e\n',relFerr);
                            end

                            % check residuals
                            test.fatalAssertLessThan(diff2nrm,test.odetol);
                            test.fatalAssertLessThan(diffFnrm,test.odetol);

                        end

                    end
                end
            end
        end


    end
end

