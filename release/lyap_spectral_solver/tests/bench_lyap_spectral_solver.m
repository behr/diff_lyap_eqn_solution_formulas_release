%% clear all
clear all, close all, clc

%% load data
%Rail = load('rail1357');
Rail = load('rail5177');

%% benchmark lyap spectral solver
profile on
spectral = lyap_spectral_solver(Rail.E,Rail.A,Rail.B);
X        = spectral.solve_lyap();
Xt       = spectral.solve_dlyap(1);
profile close
profile viewer
