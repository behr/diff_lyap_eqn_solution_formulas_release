classdef test_ldlt_solver < matlab.unittest.TestCase
    % test_ldlt_solver class is a matlab unittest.
    % The function of ldlt_lyap is tested.
    %
    % Maximilian Behr, MPI-Magdeburg

    properties
        T               = 1.0;              % Time Horizon
        ks              = {4, 6};           % Step Sizes
        solvers         = { ldlt_bdf_solver_t.BDF6, ...
            ldlt_bdf_solver_t.BDF5, ...
            ldlt_bdf_solver_t.BDF4, ...
            ldlt_bdf_solver_t.BDF3, ...
            ldlt_bdf_solver_t.BDF2, ...
            ldlt_bdf_solver_t.BDF1, ...
            ldlt_ros_solver_t.ROS2, ...
            ldlt_ros_solver_t.ROS1};
        reltol          = 1e-1;             % tolerance for relative error
        abstol          = 1e-1;             % tolerance for relative error
        output          = false;            % turn output on/off
        railinstance    = 'rail1357.mat';    % the test instance for the rial tests
    end

    methods(Test)
        function test_lyap_rail(test)

            %% load matrices
            Rail = load(test.railinstance);
            E = Rail.E;
            A = Rail.A;
            B = Rail.B;

            %% generate solver for comparison
            lyap_spectral = lyap_spectral_solver(E, A, B, true);

            %% setup parameters, solve and test relative error
            for ik = 1:length(test.ks)
                for isolver = 1:length(test.solvers)

                    % parameters
                    k = test.ks{ik};
                    h = 1/2^k;
                    solver = test.solvers{isolver};
                    name = sprintf('%s_%s_k=%d',test.railinstance,char(solver),k);

                    % solve using ldlt
                    results = ldlt_lyap(E, A, B, test.T, h, solver, test.output, name, @(t) lyap_spectral.solve_dlyap(t));

                    % print output
                    fprintf('Finished\n');
                    fprintf('Solver                 = %s\n', char(solver));
                    fprintf('T                      = %f\n', test.T);
                    fprintf('k                      = %d\n', k);
                    fprintf('h                      = %f\n', h);
                    fprintf('Rail Instance          = %s\n', test.railinstance);
                    fprintf('Max. abs. 2-Norm Error = %e\n', results.maxabsnrm2err);
                    fprintf('Max. rel. 2-Norm Error = %e\n', results.maxrelnrm2err);
                    test.fatalAssertLessThan(results.maxabsnrm2err,test.abstol);
                    test.fatalAssertLessThan(results.maxrelnrm2err,test.reltol);
                end
            end
        end
    end
end

