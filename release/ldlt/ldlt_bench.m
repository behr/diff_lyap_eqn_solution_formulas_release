% LDLT_BENCH is a script to solve the DLE using BDF or Rosenbrock methods
% and compare the results to the solution obtained by the spectral decomposition.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20016-2018
%
%
%% clear
clear all;
close all;
clc;

%% parameters
% different solvers
solvers = {
    ldlt_bdf_solver_t.BDF6, ldlt_bdf_solver_t.BDF5, ...
    ldlt_bdf_solver_t.BDF4, ldlt_bdf_solver_t.BDF3, ...
    ldlt_bdf_solver_t.BDF2, ldlt_bdf_solver_t.BDF1, ...
    ldlt_ros_solver_t.ROS2, ldlt_ros_solver_t.ROS1
    };

rails = {'rail1357',};%'rail5177'%};
ks = {8, 6, 4};  % step size is 1/2^k
transt = {mess_operation_t.MESS_OP_NONE, mess_operation_t.MESS_OP_TRANSPOSE};
T = 100;

%% iterate over different parameters
for isolver = 1:length(solvers)
    for irail = 1:length(rails)
        for ik = 1:length(ks)
            for itrans = 1:length(transt)
                
                %% setup parameters
                railname = rails{irail};
                k = ks{ik};
                h = 1/2^k;
                solver = solvers{isolver};
                info = true;
                trans = transt{itrans};
                
                %% load data
                rail = load(railname);
                if trans == mess_operation_t.MESS_OP_NONE
                    E = rail.E;
                    A = rail.A;
                    B = rail.B;
                    trans_char = 'none';
                else
                    E = rail.E';
                    A = rail.A';
                    B = rail.C';
                    trans_char = 'trans';
                end
                
                %% load spectral decomposition
                rail_spectral_decomp = load(strcat(railname,'_spectral_decomp'));
                lyap_spectral = lyap_spectral_solver(E,A,B,true,rail_spectral_decomp.VAE, rail_spectral_decomp.DAE);
                name = sprintf('%s_%s_T%f_k%d_op_%s',railname, solver, T, k, trans_char);
                
                %% solve DLE
                ldlt_lyap(E, A, B, T, h, solver, info, name, @(t) lyap_spectral.solve_dlyap(t));
                
            end
        end
    end
end

