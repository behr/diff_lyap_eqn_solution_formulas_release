% LDLT_ROS_SOLVER_T Enumeration for different BDF LDL^T solver types.
%
%   See also LDLT_BDF_SOLVER_T.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20016-2018
%
%

classdef (Sealed = true) ldlt_ros_solver_t < int8
    enumeration
        ROS1(1), % The ROS1-method.
        ROS2(2), % The ROS2-method.
    end
end
