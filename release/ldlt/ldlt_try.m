% LDLT_TRY is a script to solve the DLE using BDF or Rosenbrock methods
% and compare the results to the solution obtained by the spectral decomposition.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20016-2018
%
%
%% clear
clear all;
close all;
clc;

%% setup parameters
railname = 'rail5177'; 
k = 8;
T = 1;
h = 1/2^k;
solver = ldlt_bdf_solver_t.BDF2;
info = true;
trans = mess_operation_t.MESS_OP_NONE;

%% load data
rail = load(railname);
if trans == mess_operation_t.MESS_OP_NONE
    E = rail.E;
    A = rail.A;
    B = rail.B;
    trans_char = 'none';
else
    E = rail.E';
    A = rail.A';
    B = rail.C';
    trans_char = 'trans';
end

%% load spectral decomposition
rail_spectral_decomp = load(strcat(railname,'_spectral_decomp'));
lyap_spectral = lyap_spectral_solver(E,A,B,true,rail_spectral_decomp.VAE, rail_spectral_decomp.DAE);
name = sprintf('%s_%s_T%f_k%d_op_%s',railname, solver, T, k, trans_char);

%% solve DLE
results = ldlt_lyap(E, A, B, T, h, solver, info, name, @(t) lyap_spectral.solve_dlyap(t));

%% plot
subplot(2,1,1)
semilogy(results.tspan,results.absnrm2err)
xlabel(sprintf('t \\in [0,%d]',round(max(results.tspan))));
ylabel('|| LDL^T(t) - X_{ref}(t)||_2');
subplot(2,1,2)
semilogy(results.tspan,results.relnrm2err)
xlabel(sprintf('t \\in [0,%d]',round(max(results.tspan))));
ylabel('|| LDL^T(t) - X_{ref}(t)||_2 / || X_{ref}(t) ||_2');
disp(results);