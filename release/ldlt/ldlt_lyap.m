function results = ldlt_lyap(E,A,B,T,h,solver,info,name,get_true_solution)
%% LDLT_LYAP solver for DLE
%
%   Edot{X}E' = AXE' +EXA' + BB', X(0)=0
%
%
%  Inputs:
%    E                       - (Input) Matrix E of DLE
%    A                       - (Input) Matrix A of DLE
%    B                       - (Input) Matrix B of DLE
%    T                       - (Input) Final Time
%    h                       - (Input) Step Size
%    solver                  - (Input) Enum ldlt_bdf_solver_t or ldlt_ros_solver_t
%    info                    - (Input) Verbose or not
%    name                    - (Input) Name for Logging
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               20016-2018
%

%% check data
assert(all(size(E) == size(A)),'Matrices A and E have different sizes');
assert(0<h, 'h must be positive');
assert(0<T, 'T must be positive');
assert(h<T, 'h must be smaller than T');
assert(isa(solver,'ldlt_bdf_solver_t') || isa(solver,'ldlt_ros_solver_t'), 'solver must be a ldlt_bdf_solver_t or ldlt_ros_solver_t instance.');

%% create necessary filenames and create directory for results
%mydir   = sprintf('results/%s/%s/',mfilename,z_projection_conf.name);
[mydir,~,~] = fileparts(mfilename('fullpath'));
mydir       = fullfile(mydir,'results',mfilename,name);
mydiary     = fullfile(mydir,sprintf('%s.log',name));
mymat       = fullfile(mydir,sprintf('%s.mat',name));
mydat       = fullfile(mydir,sprintf('%s.dat',name));
mkdir(mydir);

%% delete old diary and create new one
delete(mydiary);
diary(mydiary);

%% prepare data
eqn.E = E;
eqn.F = A;
eqn.B = @(t) B; %use function handle instead of matrix, see issue.txt 
eqn.C = []; 

% initial value is X(0)=0
n = size(E,1);
eqn.L = zeros(n,1);
eqn.D = 1;

% set time span and order of solver
opts.tspan = 0:h:T;
opts.ord = double(solver);
opts.type = 'B';
opts.info = info;

%% solve DLE
fprintf('%s Start Integration using LDL^T Solver %s\n',datestr(now),char(solver));
out.walltime = 0;
tic;
switch(solver)
    case {ldlt_bdf_solver_t.BDF1, ldlt_bdf_solver_t.BDF2, ...
            ldlt_bdf_solver_t.BDF3, ldlt_bdf_solver_t.BDF4, ...
            ldlt_bdf_solver_t.BDF5, ldlt_bdf_solver_t.BDF6}
        % BDF DLE solver
        [out.L, out.D] = g_lrLDL_BDF_DLE(eqn,opts);
    case ldlt_ros_solver_t.ROS1
        % Rosenbrock 1 solver
        [out.L, out.D] = g_lrLDL_Ros1_DLE(eqn,opts);
    case ldlt_ros_solver_t.ROS2
        % Rosenbrock 1 solver
        [out.L, out.D] = g_lrLDL_Ros2_DLE(eqn,opts);
    otherwise
        error('solver not implemented\n');
end
out.walltime = toc;
fprintf('%s Finished Integration using LDL^T Solver %s in %f seconds.\n',datestr(now),char(solver),out.walltime);

%% compute error
assert(length(out.L) == length(out.D),'output from LDL^T solver seems to be incorrect');
assert(length(out.L) == length(opts.tspan),'output from LDL^T solver seems to be incorrect');

fprintf('%s Start Compute Error for %s.\n',datestr(now),char(solver));
ltspan = length(opts.tspan);

nrm2err = zeros(ltspan,1);
nrm2Xtrue = zeros(ltspan,1);
nrm2LDLT = zeros(ltspan,1);

walltime_error = 0;
tic;
for i = 1:ltspan
    fprintf('Compute Error at time step %f %d / %d\n',opts.tspan(i),i,ltspan);
    Xtrue = get_true_solution(opts.tspan(i));
    L = out.L{i};
    D = out.D{i};
    nrm2Xtrue(i) = max(abs(eigs(Xtrue, 1, 'LM')));                          % ||Xtrue||_2 Xtrue is symmetric
    nrm2err(i) = max(abs(eigs(@(x) (L*(D*(L'*x))) - Xtrue*x, n, 1, 'LM'))); % ||L*D*L^T - Xtrue||_2
    nrm2LDLT(i) = max(abs(eigs(@(x) (L*(D*(L'*x))), n, 1, 'LM')));
    t = opts.tspan(i);
    fprintf('t = %f, ||Xtrue|| = %e,\t||LDL^T -Xtrue|| = %e\t||LDL^T|| = %e\n\n', t, nrm2Xtrue(i), nrm2err(i), nrm2LDLT(i));
end
walltime_error = toc;
fprintf('%s Finished Compute Error for %s in %f seconds.\n',datestr(now),char(solver),walltime_error);

%% prepare results structure
tspan = opts.tspan;
results.time_integration    = out.walltime;
results.time_error_comp     = walltime_error;
results.solver              = char(solver);
results.tspan               = tspan;
results.absnrm2err          = nrm2err;
results.nrm2Xtrue           = nrm2Xtrue;
results.nrm2LDLT            = nrm2LDLT;
results.Lcols               = cellfun(@(Li) size(Li,2), out.L);
results.Lrows               = cellfun(@(Li) size(Li,1), out.L);
results.Lmemsize            = ByteSize(out.L);
results.Dmemsize            = ByteSize(out.D);


% relative error, set infinity to 0
temp                        = nrm2err./nrm2Xtrue; temp(isinf(temp))=0; temp(isnan(temp))=0;
results.relnrm2err          = temp;
results.maxabsnrm2err       = max(results.absnrm2err);
results.meanabsnrm2err      = mean(results.absnrm2err);

results.maxrelnrm2err       = max(results.relnrm2err);
results.meanrelnrm2err      = mean(results.relnrm2err);

hostname                    = getComputerName();
timenow                     = datestr(now);
results.hostname            = hostname;
results.timenow             = timenow;

fprintf('%s Save results to mat file\n',datestr(now));
save(mymat,'tspan','solver','hostname','timenow','results');


%% save results to text file
dlmwrite(mydat,name, 'delimiter','');
dlmwrite(mydat,timenow, 'delimiter','');
dlmwrite(mydat,hostname,'delimiter','','-append');
dlmwrite(mydat,char(solver),'delimiter','','-append');
dlmwrite(mydat,sprintf('T                = %f',max(tspan)),   'delimiter','','-append');
dlmwrite(mydat,sprintf('Integration time = %f',results.time_integration),   'delimiter','','-append');
dlmwrite(mydat,sprintf('maxabsnrm2err    = %e',results.maxabsnrm2err),      'delimiter','','-append');
dlmwrite(mydat,sprintf('meanabsnrm2err   = %e',results.meanabsnrm2err),     'delimiter','','-append');
dlmwrite(mydat,sprintf('maxrelnrm2err    = %e',results.maxrelnrm2err),      'delimiter','','-append');
dlmwrite(mydat,sprintf('meanrelnrm2err   = %e',results.meanrelnrm2err),     'delimiter','','-append');
dlmwrite(mydat,'Time, ABSNRM2ERR, RELNRM2ERR, NRM2XTRUE, NRM2LDLT','delimiter','','-append');
dlmwrite(mydat,[tspan(:), results.absnrm2err, results.relnrm2err, results.nrm2Xtrue, results.nrm2LDLT], ...
    'delimiter',',','-append','precision',16);

%% turn diary off
diary('off');









