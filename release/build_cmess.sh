#!/bin/bash

#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               20016-2018
#

scriptdir="$(dirname "$0")"
cd "$scriptdir"

CMESS_SRC=../cmess-release
CMESS_BUILD=mess_build


# create build folder
rm -rf ${CMESS_BUILD}
mkdir ${CMESS_BUILD}

# get matlab root
#MROOT=$(matlab -n | grep -P "MATLAB\s+=" | awk '{print $5}')
MROOT=$(mpi_matlab2017a -n | grep -P "MATLAB\s+=" | awk '{print $5}')


#configure build and compile mexmess
#CC=gcc-4.9 CXX=g++-4.9 FC=gfortran-4.9 cmake ${CMESS} -DDEBUG=OFF -DSUPERLU=OFF -DARPACK=ON -DOPENMP=OFF -DMATLAB=ON -DMATLAB_ROOT=${MROOT}
cd ${CMESS_BUILD}
cmake ../${CMESS_SRC} -DDEBUG=OFF -DSUPERLU=OFF -DARPACK=ON -DOPENMP=OFF -DMATLAB=ON -DMATLAB_ROOT=${MROOT}
make











