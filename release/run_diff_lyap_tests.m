%RUN_TESTS  Runs all unit tests.
%
%   RUN_TESTS() runs all unit tests
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, see <http://www.gnu.org/licenses/>.
%
%  Copyright (C) Maximilian Behr
%                2017-2018
%

function ret = run_diff_lyap_tests()
    %% import test package
    import matlab.unittest.TestSuite;

    %% create suites
    lyap_spectral   = TestSuite.fromFolder(strcat(pwd(),'/lyap_spectral_solver/tests/'));
    z_projection    = TestSuite.fromFolder(strcat(pwd(),'/z_projection/tests/'));
    ldlt            = TestSuite.fromFolder(strcat(pwd(),'/ldlt/tests/'));
    
    %% lyap_spectral suite
    suite = lyap_spectral;
    results = run(suite);
    disp(results);
    ret = any([results.Failed]);
    if ret, warning('lyap_spectral testsuite failed.'); return; end

    %% z_projection suite
    suite = z_projection;
    results = run(suite);
    disp(results);
    ret = any([results.Failed]);
    if ret, warning('z_projection testsuite failed.'); return; end
    
    %% ldlt suite
    suite = ldlt;
    results = run(suite);
    disp(results);
    ret = any([results.Failed]);
    if ret, warning('ldlt testsuite failed.'); return; end
    
    
end
